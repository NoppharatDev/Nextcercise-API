import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CheckpointDto } from './dto/checkpoint.dto';
import { CheckPoint } from './entities/checkpoint.entity';
import { v4 as uuidv4 } from 'uuid';
import { createWriteStream } from 'fs';
import { S3 } from 'aws-sdk';
var fs = require('fs');

@Injectable()
export class CheckpointsService {
  constructor(
    @InjectRepository(CheckPoint) private checkPointRepository: Repository<CheckPoint>
  ) {}

  /**************************/
  /** Create New CheckPoin **/
  /**************************/
  async create(files, checkPointDto: CheckpointDto, res): Promise<CheckPoint> {
    // console.log(checkPointDto);
    try {
      const { eId, cpId } = checkPointDto;
      const dir = `${eId}/${cpId}`;

      // if(files.backgroundFile) { checkPointDto.backgroundFile = await this.uploadFile(files.backgroundFile, dir); }
      if(files.startFile) { checkPointDto.startFile = await this.uploadFile(files.startFile, dir); }
      if(files.resultFile) { checkPointDto.resultFile = await this.uploadFile(files.resultFile, dir); }
      if(files.beforeFile) { checkPointDto.beforeFile = await this.uploadFile(files.beforeFile, dir); }
      if(files.afterFile) { checkPointDto.afterFile = await this.uploadFile(files.afterFile, dir); }
      const saveCheckPoint = await this.checkPointRepository.save(checkPointDto)

      return res.status(201).send({
        statusCode: 201,
        success: true,
        message: "Create new checkpoint successfully",
        result: saveCheckPoint
      });

    } catch (error) {
      return res.status(400).send({
        statusCode: 400,
        success: false,
        message: error.message,
        result: error
      });
    }
  }

  /******************************/
  /** Update CheckPoint Detail **/
  /******************************/
  async update(files, eId: string, cpId: string, checkPointDto: CheckpointDto, res) {
    try {
      const dir = `${eId}/${cpId}`;

      // if(files.backgroundFile) { checkPointDto.backgroundFile = await this.uploadFile(files.backgroundFile, dir); }
      if(files.startFile) { checkPointDto.startFile = await this.uploadFile(files.startFile, dir); }
      if(files.resultFile) { checkPointDto.resultFile = await this.uploadFile(files.resultFile, dir); }
      if(files.beforeFile) { checkPointDto.beforeFile = await this.uploadFile(files.beforeFile, dir); }
      if(files.afterFile) { checkPointDto.afterFile = await this.uploadFile(files.afterFile, dir); }
      const saveCheckPoint = await this.checkPointRepository.update({eId, cpId}, checkPointDto)

      return res.status(200).send({
        statusCode: 200,
        success: true,
        message: "Update checkpoint successfully.",
        result: saveCheckPoint
      });   
    } catch (error) {
      return res.status(400).send({
        statusCode: 400,
        success: false,
        message: error.message,
        result: error
      });
    }
  }

  /******************************/
  /** Start Find CheckPoin All **/
  /******************************/
  async findAll(res): Promise<CheckPoint[]> {
    try {
      const findCheckPoint = await this.checkPointRepository.find({
        order: {
          createdAt: "ASC"
        }
      })

      return res.status(200).send({
        statusCode: 200,
        success: true,
        message: "Find checkpoint all successfully.",
        result: findCheckPoint
      });
    } catch (error) {
      return res.status(400).send({
        statusCode: 400,
        success: false,
        message: error.message,
        result: error
      });
    }
  }

  /***************************************/
  /** Start Find CheckPoin All By Event **/
  /***************************************/
  async findAllByEvent(eId, res): Promise<CheckPoint[]> {
    try {
      const findCheckPoint = await this.checkPointRepository.find({
        where: {
          eId: eId
        },
        order: {
          createdAt: "ASC"
        }
      })

      return res.status(200).send({
        statusCode: 200,
        success: true,
        message: "Find checkpoint all by event successfully.",
        result: findCheckPoint
      });
    } catch (error) {
      return res.status(400).send({
        statusCode: 400,
        success: false,
        message: error.message,
        result: error
      });
    }
  }

  /*****************************/
  /** Start Delete CheckPoint **/
  /*****************************/
  async deleteCheckpoint(eId, cpId, res) {
    try {
      const deleteChackpoint = await this.checkPointRepository.delete({eId, cpId});
      return res.status(200).send({
        statusCode: 200,
        success: true,
        message: `Delete checkpoint successfully`,
        result: deleteChackpoint
      });
    } catch (error) {
      return res.status(400).send({
        statusCode: 400,
        success: false,
        message: error.message,
        result: error
      });
    }
  }

  async uploadFile(file, dir) {

    const uploadToS3 = async (file, originalname) => {
      const s3 = getS3();
      const bucketS3 = `metaxr-s3/nextcercise/${dir}`;
      return await uploadS3(file.buffer, bucketS3, originalname);

      // return await s3.listObjectsV2({
      //   Bucket: bucketS3
      // }).promise();
    }

    const uploadS3 = async (file, bucket, name) => {
      const s3 = getS3();
      const params = {
        Bucket: bucket,
        Key: String(name),
        Body: file,
        ACL: 'public-read'
      };
      return new Promise((resolve, reject) => {
        s3.upload(params, (err, data) => {
          if (err) {
            Logger.error(err);
            reject(err.message);
          }
          resolve(data);
        });
      });
    }

    const getS3 = () => {
      return new S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      });
    }

    const date = new Date().getTime();
    const uuid = uuidv4();
    const fileName = `${date}-${uuid}`;
    const originalName = file[0].originalname
    const splitOriginalName = originalName.split(".");
    const exFile = splitOriginalName[splitOriginalName.length - 1];
    const newOriginalName = `${fileName}.${exFile}`;

    const s3FileName = await uploadToS3(file[0], newOriginalName)
    return s3FileName['key'];

    // const file0 = file[0];
    // const date = new Date().getTime();
    // const uuid = uuidv4();
    // let fileName = btoa(`${date}-${uuid}`);
    // const originalName = file0.originalname;
    // const splitOriginalName = originalName.split(".");
    // const exFile = splitOriginalName[splitOriginalName.length - 1];

    // const originalname = `${fileName}.${exFile}`;
    // const path = `${dir}/${originalname}`;
    // let fileStream = createWriteStream(path);
    // fileStream.write(file0.buffer);
    // fileStream.end();

    // return originalname;
  }
}
