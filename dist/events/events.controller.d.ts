/// <reference types="multer" />
import { EventsService } from './events.service';
import { EventDto } from './dto/event.dto';
export declare class EventsController {
    private readonly eventsService;
    constructor(eventsService: EventsService);
    create(files: {
        background?: Express.Multer.File[];
        banner?: Express.Multer.File[];
        visual?: Express.Multer.File[];
    }, eventDto: EventDto, res: any): Promise<import("./entities/event.entity").Event>;
    update(files: {
        background?: Express.Multer.File[];
        banner?: Express.Multer.File[];
        visual?: Express.Multer.File[];
    }, id: string, eventDto: EventDto, res: any): Promise<any>;
    removeToTrashEvent(id: string, res: any): Promise<any>;
    restoreEvent(id: string, res: any): Promise<any>;
    findAll(res: any): Promise<import("./entities/event.entity").Event[]>;
    findOne(id: string, res: any): Promise<import("./entities/event.entity").Event>;
    findTrash(res: any): Promise<any>;
    findPublish(res: any): Promise<any>;
    deleteEvent(id: string, res: any): Promise<any>;
}
