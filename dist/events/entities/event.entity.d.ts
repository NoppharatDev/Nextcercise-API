export declare class Event {
    eId: string;
    title: string;
    periodStart: Date;
    periodEnd: Date;
    description: string;
    rewardId: string;
    hostCode: string;
    hostDetail: string;
    background: string;
    banner: string;
    visual: string;
    isPublish: boolean;
    isDraft: boolean;
    isTrash: boolean;
    isNoPath: boolean;
    createdAt: Date;
    updatedAt: Date;
}
export declare class EventDraft extends Event {
}
