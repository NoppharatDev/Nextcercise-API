import { Repository } from 'typeorm';
import { Event, EventDraft } from './entities/event.entity';
import { CheckPoint } from 'src/checkpoints/entities/checkpoint.entity';
export declare class EventsService {
    private eventRepository;
    private eventDraftRepository;
    private checkPointRepository;
    constructor(eventRepository: Repository<Event>, eventDraftRepository: Repository<EventDraft>, checkPointRepository: Repository<CheckPoint>);
    create(files: any, eventDto: any, res: any): Promise<Event>;
    update(files: any, id: string, eventDto: any, res: any): Promise<any>;
    removeToTrashEvent(eId: string, res: any): Promise<any>;
    restoreEvent(eId: string, res: any): Promise<any>;
    findAll(res: any): Promise<Event[]>;
    findOne(id: string, res: any): Promise<Event>;
    findTrash(res: any): Promise<any>;
    findPublish(res: any): Promise<any>;
    deleteEvent(eId: string, res: any): Promise<any>;
    uploadFile(file: any, dir: any): Promise<any>;
}
