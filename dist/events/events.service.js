"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const uuid_1 = require("uuid");
const event_entity_1 = require("./entities/event.entity");
const checkpoint_entity_1 = require("../checkpoints/entities/checkpoint.entity");
const aws_sdk_1 = require("aws-sdk");
var fs = require('fs');
let EventsService = class EventsService {
    constructor(eventRepository, eventDraftRepository, checkPointRepository) {
        this.eventRepository = eventRepository;
        this.eventDraftRepository = eventDraftRepository;
        this.checkPointRepository = checkPointRepository;
    }
    async create(files, eventDto, res) {
        try {
            let { isPublish, isDraft, isTrash, isNoPath } = eventDto;
            eventDto.eId = (0, uuid_1.v4)();
            if (isPublish === 'true') {
                eventDto.isPublish = true;
            }
            else if (isPublish === 'false') {
                eventDto.isPublish = false;
            }
            if (isDraft === 'true') {
                eventDto.isDraft = true;
            }
            else if (isDraft === 'false') {
                eventDto.isDraft = false;
            }
            if (isTrash === 'true') {
                eventDto.isTrash = true;
            }
            else if (isTrash === 'false') {
                eventDto.isTrash = false;
            }
            if (isNoPath === 'true') {
                eventDto.isNoPath = true;
            }
            else if (isNoPath === 'false') {
                eventDto.isNoPath = false;
            }
            if (files.background) {
                eventDto.background = await this.uploadFile(files.background, eventDto.eId);
            }
            else {
                return res.status(200).send({
                    statusCode: 200,
                    success: false,
                    message: "ไม่สามารถสร้าง Event ได้เนื่องจากไม่พบการอัพโหลด Background",
                });
            }
            if (files.banner) {
                eventDto.banner = await this.uploadFile(files.banner, eventDto.eId);
            }
            else {
                return res.status(200).send({
                    statusCode: 200,
                    success: false,
                    message: "ไม่สามารถสร้าง Event ได้เนื่องจากไม่พบการอัพโหลด Banner",
                });
            }
            if (files.visual) {
                eventDto.visual = await this.uploadFile(files.visual, eventDto.eId);
            }
            else {
                return res.status(200).send({
                    statusCode: 200,
                    success: false,
                    message: "ไม่สามารถสร้าง Event ได้เนื่องจากไม่พบการอัพโหลด Visual",
                });
            }
            const statusTrue = "true" || true;
            const statusFalse = "false" || false;
            if (isPublish === statusTrue) {
                isPublish = true;
            }
            else if (isPublish === statusFalse) {
                isPublish = false;
            }
            let saveEvent, saveEventDraft;
            console.log(eventDto);
            if (isPublish === true || isPublish === false) {
                saveEventDraft = await this.eventDraftRepository.save(Object.assign(Object.assign({}, eventDto), { isDraft: false, isTrash: false, isPublish: isPublish }));
                saveEvent = await this.eventRepository.save(Object.assign(Object.assign({}, eventDto), { isDraft: false, isTrash: false, isPublish: isPublish }));
            }
            else if (isDraft === statusTrue) {
                saveEventDraft = await this.eventDraftRepository.save(Object.assign(Object.assign({}, eventDto), { isPublish: false, isDraft: true }));
            }
            else {
                saveEventDraft = await this.eventDraftRepository.save(eventDto);
            }
            console.log(saveEvent, saveEventDraft);
            return res.status(201).send({
                statusCode: 201,
                success: true,
                message: "Create event successfully.",
                result: { saveEvent, saveEventDraft },
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error,
            });
        }
    }
    async update(files, id, eventDto, res) {
        try {
            let { isPublish, isDraft, isTrash, isNoPath } = eventDto;
            const { eId } = eventDto, newEventDto = __rest(eventDto, ["eId"]);
            eventDto.eId = (0, uuid_1.v4)();
            if (isPublish === 'true') {
                eventDto.isPublish = true;
            }
            else if (isPublish === 'false') {
                eventDto.isPublish = false;
            }
            if (isDraft === 'true') {
                eventDto.isDraft = true;
            }
            else if (isDraft === 'false') {
                eventDto.isDraft = false;
            }
            if (isTrash === 'true') {
                eventDto.isTrash = true;
            }
            else if (isTrash === 'false') {
                eventDto.isTrash = false;
            }
            if (isNoPath === 'true') {
                eventDto.isNoPath = true;
            }
            else if (isNoPath === 'false') {
                eventDto.isNoPath = false;
            }
            if (files) {
                if (files.background) {
                    newEventDto.background = await this.uploadFile(files.background, id);
                }
                if (files.banner) {
                    newEventDto.banner = await this.uploadFile(files.banner, id);
                }
                if (files.visual) {
                    newEventDto.visual = await this.uploadFile(files.visual, id);
                }
            }
            const statusTrue = "true" || true;
            const statusFalse = "false" || false;
            if (isPublish === statusTrue) {
                isPublish = true;
            }
            else if (isPublish === statusFalse) {
                isPublish = false;
            }
            let saveEvent, saveEventDraft;
            if (isPublish === true || isPublish === false) {
                const findCheckpointByEventId = await this.checkPointRepository.findOneBy({ eId: id });
                if (findCheckpointByEventId) {
                    saveEventDraft = await this.eventDraftRepository.update(id, Object.assign(Object.assign({}, newEventDto), { isDraft: false, isTrash: false, isPublish: isPublish }));
                    const findEventDraft = await this.eventDraftRepository.findOneBy({ eId: id });
                    const findEvent = await this.eventRepository.findOneBy({ eId: id });
                    if (findEvent === null) {
                        saveEvent = await this.eventRepository.save(Object.assign({}, findEventDraft));
                    }
                    else {
                        saveEvent = await this.eventRepository.update(id, Object.assign(Object.assign({}, newEventDto), { isDraft: false, isTrash: false, isPublish: isPublish }));
                    }
                }
                else {
                    return res.status(200).send({
                        statusCode: 200,
                        success: false,
                        message: "ไม่สามารถ Publish ได้ เนื่องจากไม่พบ Checkpoint ใน Event นี้",
                    });
                }
            }
            else if (isDraft === statusTrue) {
                saveEventDraft = await this.eventDraftRepository.update(id, Object.assign(Object.assign({}, newEventDto), { isPublish: false, isDraft: true }));
            }
            else {
                saveEvent = await this.eventRepository.update(id, newEventDto);
                saveEventDraft = await this.eventDraftRepository.update(id, newEventDto);
            }
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Update event successfully.",
                result: { saveEvent, saveEventDraft }
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error,
            });
        }
    }
    async removeToTrashEvent(eId, res) {
        try {
            const removeEventToTrash = await this.eventRepository.update(eId, { isTrash: true });
            const removeEventDraftToTrash = await this.eventDraftRepository.update(eId, { isTrash: true });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Remove event to trash successfully.",
                result: { removeEventToTrash, removeEventDraftToTrash },
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error,
            });
        }
    }
    async restoreEvent(eId, res) {
        try {
            const removeEventToTrash = await this.eventRepository.update(eId, { isTrash: false });
            const removeEventDraftToTrash = await this.eventDraftRepository.update(eId, { isTrash: false });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Restore event successfully.",
                result: { removeEventToTrash, removeEventDraftToTrash },
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error,
            });
        }
    }
    async findAll(res) {
        try {
            const findEvent = await this.eventDraftRepository.find({ order: { createdAt: 'DESC' } });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Find events successfully.",
                result: findEvent,
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error,
            });
        }
    }
    async findOne(id, res) {
        try {
            const findOneEvent = await this.eventDraftRepository.findOne({
                where: {
                    eId: id,
                    isTrash: false
                },
            });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Find event one successfully.",
                result: findOneEvent,
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error,
            });
        }
    }
    async findTrash(res) {
        try {
            const findAllEventInTrash = await this.eventDraftRepository.findBy({ isTrash: true });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: `Find event all in trash successfully`,
                result: findAllEventInTrash
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async findPublish(res) {
        try {
            const findAllEventInTrash = await this.eventRepository.findBy({ isPublish: true, isTrash: false, isDraft: false });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: `Find event all in publish successfully`,
                result: findAllEventInTrash
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async deleteEvent(eId, res) {
        try {
            const deleteEvent = await this.eventRepository.delete(eId);
            const deleteEventDraft = await this.eventDraftRepository.delete(eId);
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: `Delete event successfully`,
                result: { deleteEvent, deleteEventDraft }
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async uploadFile(file, dir) {
        const uploadToS3 = async (file, originalname) => {
            const s3 = getS3();
            const bucketS3 = `metaxr-s3/nextcercise/${dir}`;
            return await uploadS3(file.buffer, bucketS3, originalname);
        };
        const uploadS3 = async (file, bucket, name) => {
            const s3 = getS3();
            const params = {
                Bucket: bucket,
                Key: String(name),
                Body: file,
                ACL: 'public-read'
            };
            return new Promise((resolve, reject) => {
                s3.upload(params, (err, data) => {
                    if (err) {
                        common_1.Logger.error(err);
                        reject(err.message);
                    }
                    resolve(data);
                });
            });
        };
        const getS3 = () => {
            return new aws_sdk_1.S3({
                accessKeyId: process.env.AWS_ACCESS_KEY_ID,
                secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
            });
        };
        const date = new Date().getTime();
        const uuid = (0, uuid_1.v4)();
        const fileName = `${date}-${uuid}`;
        const originalName = file[0].originalname;
        const splitOriginalName = originalName.split(".");
        const exFile = splitOriginalName[splitOriginalName.length - 1];
        const newOriginalName = `${fileName}.${exFile}`;
        const s3FileName = await uploadToS3(file[0], newOriginalName);
        return s3FileName['key'];
    }
};
EventsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(event_entity_1.Event)),
    __param(1, (0, typeorm_1.InjectRepository)(event_entity_1.EventDraft)),
    __param(2, (0, typeorm_1.InjectRepository)(checkpoint_entity_1.CheckPoint)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], EventsService);
exports.EventsService = EventsService;
//# sourceMappingURL=events.service.js.map