"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const quests_module_1 = require("./quests/quests.module");
const admins_module_1 = require("./admins/admins.module");
const events_module_1 = require("./events/events.module");
const checkpoints_module_1 = require("./checkpoints/checkpoints.module");
const paths_module_1 = require("./paths/paths.module");
const stream_files_module_1 = require("./stream-files/stream-files.module");
const auths_module_1 = require("./auths/auths.module");
const join_events_module_1 = require("./join-events/join-events.module");
const join_event_lists_module_1 = require("./join-event-lists/join-event-lists.module");
const stream_file_findall_module_1 = require("./stream-file-findall/stream-file-findall.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({ isGlobal: true }),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                host: process.env.POSTGRE_HOST,
                port: parseInt(process.env.POSTGRE_PORT),
                username: process.env.POSTGRE_USER,
                password: process.env.POSTGRE_PASSWORD,
                database: process.env.POSTGRE_DATABASE,
                entities: ['dist/**/*.entity{.ts,.js}'],
                autoLoadEntities: true,
                synchronize: true,
            }),
            quests_module_1.QuestsModule,
            admins_module_1.AdminsModule,
            events_module_1.EventsModule,
            checkpoints_module_1.CheckpointsModule,
            paths_module_1.PathsModule,
            stream_files_module_1.StreamFilesModule,
            auths_module_1.AuthsModule,
            join_events_module_1.JoinEventsModule,
            join_event_lists_module_1.JoinEventListsModule,
            stream_file_findall_module_1.StreamFileFindallModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map