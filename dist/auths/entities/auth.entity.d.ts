export declare class Auth {
    uId: string;
    email: string;
    displayName: string;
    provider: string;
}
