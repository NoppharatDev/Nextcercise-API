import { AuthsService } from './auths.service';
import { AuthDto } from './dto/auth.dto';
export declare class AuthsController {
    private readonly authsService;
    constructor(authsService: AuthsService);
    login(authDto: AuthDto, res: any): Promise<any>;
}
