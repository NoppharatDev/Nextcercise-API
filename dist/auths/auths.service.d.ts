import { Repository } from 'typeorm';
import { AuthDto } from './dto/auth.dto';
import { Auth } from './entities/auth.entity';
export declare class AuthsService {
    private authRepository;
    constructor(authRepository: Repository<Auth>);
    login(authDto: AuthDto, res: any): Promise<any>;
}
