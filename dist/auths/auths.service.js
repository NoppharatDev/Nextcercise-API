"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const auth_entity_1 = require("./entities/auth.entity");
let AuthsService = class AuthsService {
    constructor(authRepository) {
        this.authRepository = authRepository;
    }
    async login(authDto, res) {
        try {
            const { email } = authDto;
            const findAuth = await this.authRepository.findOneBy({ email: email });
            if (findAuth) {
                return res.status(200).send({
                    statusCode: 200,
                    success: true,
                    message: "Login successfully.",
                    result: findAuth,
                });
            }
            else {
                const saveAuth = await this.authRepository.save(authDto);
                return res.status(201).send({
                    statusCode: 201,
                    success: true,
                    message: "Create new account successfully.",
                    result: saveAuth,
                });
            }
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error,
            });
        }
    }
};
AuthsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(auth_entity_1.Auth)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], AuthsService);
exports.AuthsService = AuthsService;
//# sourceMappingURL=auths.service.js.map