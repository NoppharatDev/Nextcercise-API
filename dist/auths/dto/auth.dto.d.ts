export declare class AuthDto {
    uId: string;
    email: string;
    displayName: string;
    provider: string;
}
