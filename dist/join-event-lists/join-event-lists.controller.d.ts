import { JoinEventListsService } from './join-event-lists.service';
import { JoinEventListDto } from './dto/join-event-list.dto';
export declare class JoinEventListsController {
    private readonly joinEventListsService;
    constructor(joinEventListsService: JoinEventListsService);
    generate(eId: string, joinEventListDto: JoinEventListDto, res: any): Promise<any>;
    update(jelId: string, joinEventListDto: JoinEventListDto, res: any): Promise<any>;
    findAllByEventId(jeId: string, res: any): Promise<any>;
    findOneByJoinEventList(jelId: string, res: any): Promise<any>;
}
