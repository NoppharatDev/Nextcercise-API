"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckpointsController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const swagger_1 = require("@nestjs/swagger");
const checkpoints_service_1 = require("./checkpoints.service");
const checkpoint_dto_1 = require("./dto/checkpoint.dto");
let CheckpointsController = class CheckpointsController {
    constructor(checkpointsService) {
        this.checkpointsService = checkpointsService;
    }
    create(files, checkPointDto, res) {
        return this.checkpointsService.create(files, checkPointDto, res);
    }
    update(eId, cpId, files, checkPointDto, res) {
        return this.checkpointsService.update(files, eId, cpId, checkPointDto, res);
    }
    findAll(res) {
        return this.checkpointsService.findAll(res);
    }
    findAllByEvent(eId, res) {
        return this.checkpointsService.findAllByEvent(eId, res);
    }
    deleteCheckpoint(eId, cpId, res) {
        return this.checkpointsService.deleteCheckpoint(eId, cpId, res);
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileFieldsInterceptor)([{ name: 'startFile' }, { name: 'resultFile' }, { name: 'beforeFile' }, { name: 'afterFile' }])),
    __param(0, (0, common_1.UploadedFiles)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, checkpoint_dto_1.CheckpointDto, Object]),
    __metadata("design:returntype", void 0)
], CheckpointsController.prototype, "create", null);
__decorate([
    (0, common_1.Patch)(':eId/:cpId'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileFieldsInterceptor)([{ name: 'startFile' }, { name: 'resultFile' }, { name: 'beforeFile' }, { name: 'afterFile' }])),
    __param(0, (0, common_1.Param)('eId')),
    __param(1, (0, common_1.Param)('cpId')),
    __param(2, (0, common_1.UploadedFiles)()),
    __param(3, (0, common_1.Body)()),
    __param(4, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object, checkpoint_dto_1.CheckpointDto, Object]),
    __metadata("design:returntype", void 0)
], CheckpointsController.prototype, "update", null);
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], CheckpointsController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)('event/:eId'),
    __param(0, (0, common_1.Param)('eId')),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", void 0)
], CheckpointsController.prototype, "findAllByEvent", null);
__decorate([
    (0, common_1.Delete)('delete/:eId/:cpId'),
    __param(0, (0, common_1.Param)('eId')),
    __param(1, (0, common_1.Param)('cpId')),
    __param(2, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", void 0)
], CheckpointsController.prototype, "deleteCheckpoint", null);
CheckpointsController = __decorate([
    (0, swagger_1.ApiTags)('Checkpoints'),
    (0, common_1.Controller)('checkpoint'),
    __metadata("design:paramtypes", [checkpoints_service_1.CheckpointsService])
], CheckpointsController);
exports.CheckpointsController = CheckpointsController;
//# sourceMappingURL=checkpoints.controller.js.map