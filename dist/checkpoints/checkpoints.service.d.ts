import { Repository } from 'typeorm';
import { CheckpointDto } from './dto/checkpoint.dto';
import { CheckPoint } from './entities/checkpoint.entity';
export declare class CheckpointsService {
    private checkPointRepository;
    constructor(checkPointRepository: Repository<CheckPoint>);
    create(files: any, checkPointDto: CheckpointDto, res: any): Promise<CheckPoint>;
    update(files: any, eId: string, cpId: string, checkPointDto: CheckpointDto, res: any): Promise<any>;
    findAll(res: any): Promise<CheckPoint[]>;
    findAllByEvent(eId: any, res: any): Promise<CheckPoint[]>;
    deleteCheckpoint(eId: any, cpId: any, res: any): Promise<any>;
    uploadFile(file: any, dir: any): Promise<any>;
}
