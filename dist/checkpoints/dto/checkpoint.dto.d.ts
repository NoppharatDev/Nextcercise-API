export declare class CheckpointDto {
    eId: string;
    cpId: string;
    qId: number;
    name: string;
    summary: string;
    desciption: string;
    lat: number;
    long: number;
    length: number;
    positionX: number;
    positionY: number;
    startFile: string;
    resultFile: string;
    beforeFile: string;
    afterFile: string;
    createdAt: Date;
    updatedAt: Date;
}
