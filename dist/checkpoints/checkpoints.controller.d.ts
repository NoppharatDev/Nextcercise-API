/// <reference types="multer" />
import { CheckpointsService } from './checkpoints.service';
import { CheckpointDto } from './dto/checkpoint.dto';
export declare class CheckpointsController {
    private readonly checkpointsService;
    constructor(checkpointsService: CheckpointsService);
    create(files: {
        startFile?: Express.Multer.File[];
        resultFile?: Express.Multer.File[];
        beforeFile?: Express.Multer.File[];
        afterFile?: Express.Multer.File[];
    }, checkPointDto: CheckpointDto, res: any): Promise<import("./entities/checkpoint.entity").CheckPoint>;
    update(eId: string, cpId: string, files: {
        startFile?: Express.Multer.File[];
        resultFile?: Express.Multer.File[];
        beforeFile?: Express.Multer.File[];
        afterFile?: Express.Multer.File[];
    }, checkPointDto: CheckpointDto, res: any): Promise<any>;
    findAll(res: any): Promise<import("./entities/checkpoint.entity").CheckPoint[]>;
    findAllByEvent(eId: string, res: any): Promise<import("./entities/checkpoint.entity").CheckPoint[]>;
    deleteCheckpoint(eId: string, cpId: string, res: any): Promise<any>;
}
