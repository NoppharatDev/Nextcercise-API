"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckpointsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const checkpoint_entity_1 = require("./entities/checkpoint.entity");
const uuid_1 = require("uuid");
const aws_sdk_1 = require("aws-sdk");
var fs = require('fs');
let CheckpointsService = class CheckpointsService {
    constructor(checkPointRepository) {
        this.checkPointRepository = checkPointRepository;
    }
    async create(files, checkPointDto, res) {
        try {
            const { eId, cpId } = checkPointDto;
            const dir = `${eId}/${cpId}`;
            if (files.startFile) {
                checkPointDto.startFile = await this.uploadFile(files.startFile, dir);
            }
            if (files.resultFile) {
                checkPointDto.resultFile = await this.uploadFile(files.resultFile, dir);
            }
            if (files.beforeFile) {
                checkPointDto.beforeFile = await this.uploadFile(files.beforeFile, dir);
            }
            if (files.afterFile) {
                checkPointDto.afterFile = await this.uploadFile(files.afterFile, dir);
            }
            const saveCheckPoint = await this.checkPointRepository.save(checkPointDto);
            return res.status(201).send({
                statusCode: 201,
                success: true,
                message: "Create new checkpoint successfully",
                result: saveCheckPoint
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async update(files, eId, cpId, checkPointDto, res) {
        try {
            const dir = `${eId}/${cpId}`;
            if (files.startFile) {
                checkPointDto.startFile = await this.uploadFile(files.startFile, dir);
            }
            if (files.resultFile) {
                checkPointDto.resultFile = await this.uploadFile(files.resultFile, dir);
            }
            if (files.beforeFile) {
                checkPointDto.beforeFile = await this.uploadFile(files.beforeFile, dir);
            }
            if (files.afterFile) {
                checkPointDto.afterFile = await this.uploadFile(files.afterFile, dir);
            }
            const saveCheckPoint = await this.checkPointRepository.update({ eId, cpId }, checkPointDto);
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Update checkpoint successfully.",
                result: saveCheckPoint
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async findAll(res) {
        try {
            const findCheckPoint = await this.checkPointRepository.find({
                order: {
                    createdAt: "ASC"
                }
            });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Find checkpoint all successfully.",
                result: findCheckPoint
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async findAllByEvent(eId, res) {
        try {
            const findCheckPoint = await this.checkPointRepository.find({
                where: {
                    eId: eId
                },
                order: {
                    createdAt: "ASC"
                }
            });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: "Find checkpoint all by event successfully.",
                result: findCheckPoint
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async deleteCheckpoint(eId, cpId, res) {
        try {
            const deleteChackpoint = await this.checkPointRepository.delete({ eId, cpId });
            return res.status(200).send({
                statusCode: 200,
                success: true,
                message: `Delete checkpoint successfully`,
                result: deleteChackpoint
            });
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
    async uploadFile(file, dir) {
        const uploadToS3 = async (file, originalname) => {
            const s3 = getS3();
            const bucketS3 = `metaxr-s3/nextcercise/${dir}`;
            return await uploadS3(file.buffer, bucketS3, originalname);
        };
        const uploadS3 = async (file, bucket, name) => {
            const s3 = getS3();
            const params = {
                Bucket: bucket,
                Key: String(name),
                Body: file,
                ACL: 'public-read'
            };
            return new Promise((resolve, reject) => {
                s3.upload(params, (err, data) => {
                    if (err) {
                        common_1.Logger.error(err);
                        reject(err.message);
                    }
                    resolve(data);
                });
            });
        };
        const getS3 = () => {
            return new aws_sdk_1.S3({
                accessKeyId: process.env.AWS_ACCESS_KEY_ID,
                secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
            });
        };
        const date = new Date().getTime();
        const uuid = (0, uuid_1.v4)();
        const fileName = `${date}-${uuid}`;
        const originalName = file[0].originalname;
        const splitOriginalName = originalName.split(".");
        const exFile = splitOriginalName[splitOriginalName.length - 1];
        const newOriginalName = `${fileName}.${exFile}`;
        const s3FileName = await uploadToS3(file[0], newOriginalName);
        return s3FileName['key'];
    }
};
CheckpointsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(checkpoint_entity_1.CheckPoint)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], CheckpointsService);
exports.CheckpointsService = CheckpointsService;
//# sourceMappingURL=checkpoints.service.js.map