"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckPoint = void 0;
const typeorm_1 = require("typeorm");
let CheckPoint = class CheckPoint {
};
__decorate([
    (0, typeorm_1.Column)({ primary: true }),
    __metadata("design:type", String)
], CheckPoint.prototype, "eId", void 0);
__decorate([
    (0, typeorm_1.Column)({ primary: true }),
    __metadata("design:type", String)
], CheckPoint.prototype, "cpId", void 0);
__decorate([
    (0, typeorm_1.Column)({ primary: true }),
    __metadata("design:type", Number)
], CheckPoint.prototype, "qId", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], CheckPoint.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], CheckPoint.prototype, "summary", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], CheckPoint.prototype, "desciption", void 0);
__decorate([
    (0, typeorm_1.Column)("double precision"),
    __metadata("design:type", typeorm_1.Double)
], CheckPoint.prototype, "lat", void 0);
__decorate([
    (0, typeorm_1.Column)("double precision"),
    __metadata("design:type", typeorm_1.Double)
], CheckPoint.prototype, "long", void 0);
__decorate([
    (0, typeorm_1.Column)("double precision"),
    __metadata("design:type", typeorm_1.Double)
], CheckPoint.prototype, "length", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'double precision' }),
    __metadata("design:type", typeorm_1.Double)
], CheckPoint.prototype, "positionX", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'double precision' }),
    __metadata("design:type", typeorm_1.Double)
], CheckPoint.prototype, "positionY", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], CheckPoint.prototype, "startFile", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], CheckPoint.prototype, "resultFile", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: "" }),
    __metadata("design:type", String)
], CheckPoint.prototype, "beforeFile", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: "" }),
    __metadata("design:type", String)
], CheckPoint.prototype, "afterFile", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], CheckPoint.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], CheckPoint.prototype, "updatedAt", void 0);
CheckPoint = __decorate([
    (0, typeorm_1.Entity)("checkpoint")
], CheckPoint);
exports.CheckPoint = CheckPoint;
//# sourceMappingURL=checkpoint.entity.js.map