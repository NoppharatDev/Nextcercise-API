import { Double } from "typeorm";
export declare class CheckPoint {
    eId: string;
    cpId: string;
    qId: number;
    name: string;
    summary: string;
    desciption: string;
    lat: Double;
    long: Double;
    length: Double;
    positionX: Double;
    positionY: Double;
    startFile: string;
    resultFile: string;
    beforeFile: string;
    afterFile: string;
    createdAt: Date;
    updatedAt: Date;
}
