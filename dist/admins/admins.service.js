"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminsService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const admin_entity_1 = require("./entities/admin.entity");
let AdminsService = class AdminsService {
    constructor(jwtService, adminRepository) {
        this.jwtService = jwtService;
        this.adminRepository = adminRepository;
    }
    async login(adminDto, res) {
        try {
            const findAdmin = await this.adminRepository.findOneBy(adminDto);
            if (findAdmin) {
                const { aId, email } = findAdmin;
                const jwt = await this.jwtService.signAsync({
                    aId: aId,
                    email: email,
                });
                return res.status(200).send({
                    statusCode: 200,
                    success: true,
                    message: `Login successfully!`,
                    result: {
                        authToken: jwt
                    }
                });
            }
            else {
                return res.status(200).send({
                    statusCode: 200,
                    success: false,
                    message: `Login failed! Not Found...`,
                    result: findAdmin
                });
            }
        }
        catch (error) {
            return res.status(400).send({
                statusCode: 400,
                success: false,
                message: error.message,
                result: error
            });
        }
    }
};
AdminsService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(admin_entity_1.Admin)),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        typeorm_2.Repository])
], AdminsService);
exports.AdminsService = AdminsService;
//# sourceMappingURL=admins.service.js.map