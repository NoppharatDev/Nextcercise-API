import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { AdminDto } from './dto/admin.dto';
import { Admin } from './entities/admin.entity';
export declare class AdminsService {
    private jwtService;
    private adminRepository;
    constructor(jwtService: JwtService, adminRepository: Repository<Admin>);
    login(adminDto: AdminDto, res: any): Promise<any>;
}
