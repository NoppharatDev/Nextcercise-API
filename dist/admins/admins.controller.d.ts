import { AdminsService } from './admins.service';
import { AdminDto } from './dto/admin.dto';
export declare class AdminsController {
    private readonly adminsService;
    constructor(adminsService: AdminsService);
    login(adminDto: AdminDto, res: any): Promise<any>;
}
